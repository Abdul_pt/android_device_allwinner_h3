#
# Copyright (C) 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
DEVICE_FOLDER := device/allwinner/h3

PRODUCT_AAPT_CONFIG += normal large tvdpi hdpi
PRODUCT_AAPT_PREF_CONFIG := tvdpi

PRODUCT_CHARACTERISTICS := tablet
PRODUCT_TAGS += dalvik.gc.type-precise
DEVICE_PACKAGE_OVERLAYS += $(DEVICE_FOLDER)/overlay

# Hardware-specific features
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.faketouch.xml:system/etc/permissions/android.hardware.faketouch.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
    frameworks/native/data/etc/android.hardware.camera.autofocus.xml:system/etc/permissions/android.hardware.camera.autofocus.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.distinct.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.distinct.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.xml:system/etc/permissions/android.hardware.touchscreen.xml \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    $(DEVICE_FOLDER)/prebuilt/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml

# Ramdisk
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/rootdir/fstab.sun8i:root/fstab.sun8i \
    $(DEVICE_FOLDER)/rootdir/init.sun8i.rc:root/init.sun8i.rc \
    $(DEVICE_FOLDER)/rootdir/init.sun8i.usb.rc:root/init.sun8i.usb.rc \
    $(DEVICE_FOLDER)/rootdir/ueventd.sun8i.rc:root/ueventd.sun8i.rc \
    $(DEVICE_FOLDER)/recovery/root/sbin/init_partition.sh:root/sbin/init_partition.sh \
    $(DEVICE_FOLDER)/recovery/root/sbin/usb.sh:root/sbin/usb.sh

# Prebuilts
PRODUCT_COPY_FILES += \
    $(DEVICE_FOLDER)/prebuilt/mali.ko:root/mali.ko \
    $(DEVICE_FOLDER)/prebuilt/nand.ko:root/nand.ko \
    $(DEVICE_FOLDER)/prebuilt/gpio-sunxi.ko:root/gpio-sunxi.ko

# HAL
PRODUCT_PACKAGES += \
    power.$(TARGET_BOARD_PLATFORM) \
    gralloc.$(TARGET_BOARD_HARDWARE) \
    hwcomposer.$(TARGET_BOARD_HARDWARE) \
    akmd

# Audio
PRODUCT_PACKAGES += \
    libasound \
    alsa.default \
    acoustics.default \
    libtinyalsa \
	alsa.audio.primary.$(TARGET_BOARD_PLATFORM) \
	alsa.audio_policy.$(TARGET_BOARD_PLATFORM) \
    audio_policy.$(TARGET_BOARD_PLATFORM) \
    audio.primary.$(TARGET_BOARD_PLATFORM) \
    audio.a2dp.default\
    audio.r_submix.default\
    audio.usb.default

PRODUCT_PROPERTY_OVERRIDES += \
    ro.audio.flinger_standbytime_ms=1000

$(call inherit-product-if-exists, $(LOCAL_PATH)/audio/alsa-lib/copy.mk)
$(call inherit-product-if-exists, $(LOCAL_PATH)/audio/alsa-utils/copy.mk)

# Other
PRODUCT_PACKAGES += \
    make_ext4fs \
    resize2fs \
    setup_fs \
    utility_make_ext4fs \
    libstagefrighthw \
    com.android.future.usb.accessory \
    mke2fs \
    e2fsck \
    tune2fs \
    mkdosfs

# Live Wallpapers
PRODUCT_PACKAGES += \
    LiveWallpapersPicker \
    NoiseField \
    PhaseBeam \
    librs_jni \
    libjni_pinyinime

# Default props
PRODUCT_PROPERTY_OVERRIDES += \
    cm.updater.uri=http://Freaktab.com/CyanogenModOTA/api \
    ro.opengles.version=131072 \
    debug.hwui.render_dirty_regions=false \
    wifi.interface=wlan0 \
    wifi.supplicant_scan_interval=15 \
    dalvik.vm.debug.alloc=0 \
    dalvik.vm.dexopt-data-only=1 \
    ro.sf.lcdc_composer=1 \
    persist.audio.currentplayback=9 \
    persist.audio.lastsocplayback=9 \
    persist.sys.strictmode.visual=false \
    dalvik.vm.jniopts=warnonly \
    ro.sf.lcd_density=240 \
    ro.carrier=wifi-only \
    drm.service.enabled=true \
    media.aac_51_output_enabled=true


# DRM
PRODUCT_PACKAGES += \
	drmservice \
	libdrmdecrypt \
	libdrmwvmplugin \
	libwvdrm_L3 \
	libwvdrmengine \
	libwvm \
	libWVStreamControlAPI_L3 \
	com.google.widevine.software.drm.jar \
	com.google.widevine.software.drm.xml

PRODUCT_PACKAGES += \
    displayd

PRODUCT_PACKAGES += \
	libyuvtorgb

# Set default USB interface
PRODUCT_PROPERTY_OVERRIDES += \
        service.adb.root=1 \
        ro.secure=0 \
        ro.allow.mock.location=1 \
        ro.debuggable=1 \
        persist.sys.usb.config=mtp

# Dalvik heap
$(call inherit-product, frameworks/native/build/tablet-7in-hdpi-1024-dalvik-heap.mk)

# Broadcom firmware
WIFI_BAND := 802_11_BG
$(call inherit-product-if-exists, hardware/broadcom/wlan/bcmdhd/firmware/bcm4329/device-bcm.mk)

# Gapps
BUILD_WITH_GAPPS := true

ifeq ($(BUILD_WITH_GAPPS),true)
include $(DEVICE_FOLDER)/gapps/gapps.mk
endif

include $(DEVICE_FOLDER)/vendor-blobs.mk
