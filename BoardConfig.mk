#
# Copyright (C) 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

DEVICE_FOLDER := device/allwinner/h3

# Inherit from the proprietary version
$(call inherit-product-if-exists, vendor/allwinner/h3/BoardConfigVendor.mk)

TARGET_NO_RADIOIMAGE := true
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true
TARGET_BOARD_PLATFORM := dolphin
TARGET_BOARD_HARDWARE := sun8i

TARGET_CPU_ABI  := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a7
ARCH_ARM_HAVE_TLS_REGISTER := true

#TARGET_GLOBAL_CFLAGS += -mtune=cortex-a7 -mfpu=neon -mfloat-abi=softfp
#TARGET_GLOBAL_CPPFLAGS += -mtune=cortex-a7 -mfpu=neon -mfloat-abi=softfp

TARGET_GLOBAL_CFLAGS += -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=softfp
TARGET_GLOBAL_CPPFLAGS += -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=softfp

TARGET_BOOTLOADER_BOARD_NAME := H3
TARGET_OTA_ASSERT_DEVICE := h3, H3

TARGET_SPECIFIC_HEADER_PATH += device/allwinner/h3/include

# Bluetooth
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true
BOARD_BLUEDROID_VENDOR_CONF := $(DEVICE_FOLDER)/bluetooth/vnd_h3.txt
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := $(DEVICE_FOLDER)/bluetooth

# Wi-Fi
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd
BOARD_WLAN_DEVICE           := bcmdhd
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/bcmdhd/parameters/firmware_path"
WIFI_DRIVER_FW_PATH_STA     := "/system/etc/firmware/fw_bcm4329.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/system/etc/firmware/fw_bcm4329_p2p.bin"
WIFI_DRIVER_FW_PATH_AP      := "/system/etc/firmware/fw_bcm4329_apsta.bin"

# Audio
BOARD_CODEC_ITV := true
BOARD_USES_ALSA_AUDIO := true
BUILD_WITH_ALSA_UTILS := true

# Partitions
BOARD_BOOTIMAGE_PARTITION_SIZE := 16777216
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 32777216
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 576716800
BOARD_USERDATAIMAGE_PARTITION_SIZE := 106032128
BOARD_FLASH_BLOCK_SIZE := 16384

BOARD_USES_HWCOMPOSER := true
TARGET_USES_ION := true
BOARD_EGL_CFG := $(DEVICE_FOLDER)/egl.cfg
USE_OPENGL_RENDERER := true
ENABLE_WEBGL := true
TARGET_FORCE_CPU_UPLOAD := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

TARGET_USE_QCOM_BIONIC_OPTIMIZATION := true
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := true

# Camera
USE_CAMERA_STUB := false
BOARD_NEEDS_MEMORYHEAPPMEM := true

# Kernel
#TARGET_KERNEL_CONFIG := cyanogenmod_nitrobox_defconfig
#TARGET_KERNEL_SOURCE := kernel/rockchip/rk3188
#BOARD_USES_UNCOMPRESSED_BOOT := true
#BOARD_KERNEL_IMAGE_NAME := Image
#BOARD_KERNEL_BASE := 0x60408000
BOARD_KERNEL_PAGESIZE := 16384
TARGET_PREBUILT_RECOVERY_KERNEL := $(DEVICE_FOLDER)/kernel
TARGET_PREBUILT_KERNEL := $(DEVICE_FOLDER)/kernel
ifeq ($(TARGET_PREBUILT_KERNEL),)
	LOCAL_KERNEL := $(DEVICE_FOLDER)/kernel
else
	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif

PRODUCT_COPY_FILES += \
    $(LOCAL_KERNEL):kernel

BOARD_MKBOOTIMG_ARGS := --base 0x40000000

# Recovery
RECOVERY_VARIANT := twrp
# TARGET_RECOVERY_INITRC := $(DEVICE_FOLDER)/rootdir/init.recovery.cwm.rc
TARGET_RECOVERY_INITRC := $(DEVICE_FOLDER)/rootdir/init.recovery.twrp.rc

RECOVERY_SDCARD_ON_DATA := true
BOARD_UMS_LUNFILE := /sys/devices/virtual/android_usb/android0/f_mass_storage/lun
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_HAS_LARGE_FILESYSTEM := true
BOARD_HAS_NO_SELECT_BUTTON := true
TARGET_RECOVERY_PIXEL_FORMAT := "RGB_565"
RECOVERY_FSTAB_VERSION := 2
TARGET_RECOVERY_FSTAB := $(DEVICE_FOLDER)/recovery/root/etc/recovery.fstab
BOARD_HAS_NO_REAL_SDCARD := true

# CWM
RECOVERY_NAME := CWM-based Recovery by abdul
#BOARD_CUSTOM_RECOVERY_KEYMAPPING := ../../$(DEVICE_FOLDER)/recovery_keys.c
#BOARD_CUSTOM_GRAPHICS := ../../../device/rockchip/nitrobox/recovery/graphics.c graphics_overlay.c

# TWRP
TW_THEME := landscape_hdpi
TW_ROUND_SCREEN := true
TW_INCLUDE_CRYPTO := true
TW_NO_BATT_PERCENT := true
TW_NO_SCREEN_TIMEOUT := true
TW_NO_SCREEN_BLANK := true
TW_USE_MODEL_HARDWARE_ID_FOR_DEVICE_ID := true
TWHAVE_SELINUX := true
TW_INCLUDE_FB2PNG := true
TW_NO_CPU_TEMP := true
TW_INCLUDE_NTFS_3G := true