# Boot animation
TARGET_SCREEN_HEIGHT := 1080
TARGET_SCREEN_WIDTH := 1920

# Inherit some common CyanogenMod stuff
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)

# Inherit device configuration
$(call inherit-product, device/allwinner/h3/full_h3.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := cm_h3
# Set product name
PRODUCT_BUILD_PROP_OVERRIDES += \
                       PRODUCT_NAME=H3 \
                       BUILD_UTC_DATE=0
