#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

BLOBS_PATH := device/allwinner/h3/rootdir/system

# bin
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/bin,system/bin)

# lib
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/lib,system/lib)

# egl
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/lib/egl,system/lib/egl)

# hw
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/lib/hw,system/lib/hw)

# modules
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/lib/modules,system/lib/modules)

# keylayout
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/usr/keylayout,system/usr/keylayout)

# vendor lib
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/vendor/lib,system/vendor/lib) \
    $(BLOBS_PATH)/vendor/lib/drm/libdrmwvmplugin.so:system/vendor/lib/drm/libdrmwvmplugin.so \
    $(BLOBS_PATH)/vendor/lib/mediadrm/libwvdrmengine.so:system/vendor/lib/mediadrm/libwvdrmengine.so

# vendor modules
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(BLOBS_PATH)/vendor/modules,system/vendor/modules)